import Contenido from './components/contenido'
import Perfilangel from './components/Perfilangel'

function App() {


  return (
    <>
    <Perfilangel></Perfilangel>
    <Contenido></Contenido>
    </>
  )
}

export default App
