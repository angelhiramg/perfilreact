import './Contenido.css';
import Mapachu from '../assets/Mapachu.jpg';
import animacionkirby from '../assets/animacionkirby.gif';
import amongusnalgon from '../assets/amongusnalgon.jpeg';
import investigacion from '../assets/investigacion.png';
import Panavideño from '../assets/Panavideño.jpg';

function Contenido(){
    return(
        <>
        <div className="container1">
            <img src={Mapachu} alt="" className="imagen2" />
            <img src={animacionkirby} alt="" className="imagen1" />
            <img src={amongusnalgon} alt="" className="imagen1" />
        </div>

        <div className="container2">
            <img src={investigacion} alt="" className="imagen3" />
            <img src={Panavideño} alt="" className="imagen1" />
        </div>
        </>
    )
}

export default Contenido;