import './Perfilangel.css';
import fotomia from '../assets/fotomia.jpeg';


function Perfilangel(){
    return(
        <>
        <div className="card">
            <div className="profile-picture">
                <img src={fotomia} alt="" className="profile-picture-outer-radius"/>
            </div>
        
        <div className="infobox">
            <p className="infobox-username">Guzmán Gómez Angel Hiramg</p>
            <div>
                <p className="infobox-ocupacion">Estudiante del Tecnológico de Cuautla</p>
            </div>
        
        <div className="clear-float">
        <p className="align-left">Creatividad:</p> <p className="align-right">Buena</p>
        
        </div>
        </div>
        </div>
        </>
    )
}

export default Perfilangel;